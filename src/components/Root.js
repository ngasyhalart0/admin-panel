import { Dialog } from '@blueprintjs/core'
import React from 'react'
import { Router } from 'react-router'
import { Route } from 'react-router-dom'

import { createBrowserHistory } from 'history'
import { AuthenticationState, login } from '../api/auth'
import CONFIG from '../config.json'
import Broadcast from '../views/Broadcast'
import Domains from '../views/Domains'
import Files from '../views/Files'
import MyDomains from '../views/MyDomains'
import UserPage from '../views/User'
import Users from '../views/Users'
import SettingsPage from '../views/Settings'
import Authenticator from './Authenticator'
import DialogContent from './DialogContent'
import Navigation from './Navigation'
import { Page, PageWrapper } from './Page'

export default class Root extends React.Component {
  constructor(props) {
    super(props)

    this.history = createBrowserHistory({
      basename: CONFIG.basename,
    })
  }

  state = {
    auth: null,
  }

  async componentDidMount() {
    const token = localStorage.getItem('token')

    if (token == null) {
      return
    }

    // Let's try to login immediately with the token from storage.

    const result = await login(token)
    this.setState({ auth: result.state })
  }

  canShowDialog() {
    // There's a token in localStorage, but it hasn't been tried yet, so hide
    // the dialog to prevent showing it too early.
    if (localStorage.getItem('token') != null && this.state.auth == null) {
      return false
    }

    // If we are in the indeterminate state or invalid state, show the login
    // dialog.
    return (
      this.state.auth == null || this.state.auth === AuthenticationState.INVALID
    )
  }

  renderDialogs() {
    return (
      <Dialog title="Authentication" isOpen={this.canShowDialog()}>
        <DialogContent>
          <Authenticator onLogin={this.handleLogin} />
        </DialogContent>
      </Dialog>
    )
  }

  render() {
    if (this.canShowDialog()) {
      return this.renderDialogs()
    }

    // At this point, our authentication has to be determinate due to the early
    // return.
    const auth = this.state.auth

    return (
      <div className="router-wrapper">
        <Router history={this.history}>
          <PageWrapper>
            <Navigation onTokenChanged={this.handleLogin} authState={auth} />
            <Page>
              <Route path="/domains" exact component={Domains} />
              <Route path="/domains/mine" exact component={MyDomains} />
              <Route path="/users" exact component={Users} />
              <Route path="/user/:id" exact component={UserPage} />
              <Route path="/files/:id?" exact component={Files} />
              <Route path="/broadcast" exact component={Broadcast} />
              <Route path="/settings" exact component={SettingsPage} />
            </Page>
          </PageWrapper>
        </Router>
      </div>
    )
  }

  handleLogin = (newState) => {
    console.log('[Root] Token login:', newState)
    this.setState({ auth: newState })

    // If we logged in as a user, kick us back to /. This is to make sure that
    // we aren't in any admin routes.
    if (newState === AuthenticationState.USER) {
      this.history.replace('/')
    }
  }
}

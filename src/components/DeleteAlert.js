import { Alert, Intent } from '@blueprintjs/core'
import React from 'react'

export default function DeleteAlert(props) {
  return (
    <Alert
      cancelButtonText="Cancel"
      confirmButtonText="Delete"
      icon="trash"
      intent={Intent.DANGER}
      canEscapeKeyCancel
      canOutsideClickCancel
      {...props}
    />
  )
}

export function custom(render) {
  return (props) => <DeleteAlert {...props}>{render(props)}</DeleteAlert>
}

import { ControlGroup, HTMLSelect, InputGroup } from '@blueprintjs/core'
import React from 'react'
import PropTypes from 'prop-types'

export const ItemType = {
  FILE: 'File',
  SHORTEN: 'Shorten',
}

export default class ItemSearcher extends React.Component {
  static propTypes = {
    currentType: PropTypes.oneOf(Object.values(ItemType)).isRequired,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    onItemTypeChange: PropTypes.func,
    value: PropTypes.string.isRequired,
  }

  render() {
    return (
      <ControlGroup fill>
        <HTMLSelect
          large
          className="bp3-fixed"
          onChange={this.handleTypeChange}
          value={this.props.currentType}
          disabled={this.props.disabled}
        >
          <option>File</option>
          <option>Shorten</option>
        </HTMLSelect>
        <InputGroup
          leftIcon="search"
          placeholder="Shortcode..."
          large={true}
          value={this.props.value}
          onChange={this.handleQueryChange}
          disabled={this.props.disabled}
        />
      </ControlGroup>
    )
  }

  handleQueryChange = (event) => {
    const { value } = event.currentTarget

    if (this.props.onChange) {
      this.props.onChange(value)
    }
  }

  handleTypeChange = (event) => {
    const { value } = event.currentTarget

    if (this.props.onItemTypeChange) {
      const newType = value === 'Shorten' ? ItemType.SHORTEN : ItemType.FILE
      this.props.onItemTypeChange(newType)
    }
  }
}

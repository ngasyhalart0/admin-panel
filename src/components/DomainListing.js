import React from 'react'
import PropTypes from 'prop-types'
import 'styled-components/macro'

import Domain from './Domain'

export default function DomainListing({
  userFacing = false,
  domains,
  onDelete,
  onEdit,
}) {
  if (domains.length === 0) {
    return <div css="margin: 1rem 0">Nothing here.</div>
  }

  return domains.map((domain) => {
    return (
      <Domain
        key={domain.id}
        domain={domain}
        userFacing={userFacing}
        onDelete={onDelete.bind(undefined, domain)}
        onEdit={onEdit.bind(undefined, domain)}
      />
    )
  })
}

DomainListing.propTypes = {
  domains: PropTypes.arrayOf(PropTypes.object).isRequired,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func,
  userFacing: PropTypes.bool,
}

DomainListing.defaultProps = {
  onDelete: () => {},
  onEdit: () => {},
}

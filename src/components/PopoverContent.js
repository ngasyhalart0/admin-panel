import styled from 'styled-components'

const PopoverContent = styled.div`
  max-width: 30vw;
  padding: 1rem;
`

export default PopoverContent

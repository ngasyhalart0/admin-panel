import { HTMLTable, Intent, Tag } from '@blueprintjs/core'
import React from 'react'
import styled from 'styled-components'

const InfoTable = styled(HTMLTable)`
  margin: 2rem 0 0;

  th,
  td,
  tr {
    box-shadow: none !important;
  }
`

export default function UserInfoTable({ user }) {
  const { email, consented, paranoid, domain, subdomain, limits } = user

  if (user.name === 'dummy' && user.id === '0') {
    return <p>Dummy user, no info here.</p>
  }

  const consentTag =
    consented === true ? (
      <Tag intent={Intent.SUCCESS} icon="tick">
        Consented
      </Tag>
    ) : consented == null ? (
      <Tag intent={Intent.WARNING} icon="help">
        To be determined
      </Tag>
    ) : (
      <Tag intent={Intent.DANGER} icon="cross">
        Forbidden
      </Tag>
    )

  const { uploadLimit, uploadUsed, shortensUsed, shortenLimit } = limits

  const uploadPercentage = Math.floor((uploadUsed / uploadLimit) * 100)
  const shortensPercentage = Math.floor((shortensUsed / shortenLimit) * 100)
  const mibUploaded = Math.floor(uploadUsed / (1024 * 1024))
  const mibLimit = Math.floor(uploadLimit / (1024 * 1024))

  return (
    <InfoTable>
      <tbody>
        <tr>
          <th>Email</th>
          <td>{email}</td>
        </tr>
        <tr>
          <th>Statistics participation</th>
          <td>{consentTag}</td>
        </tr>
        <tr>
          <th>Paranoid mode</th>
          <td>{paranoid ? 'enabled' : 'disabled'}</td>
        </tr>
        <tr>
          <th>Domain</th>
          <td>{domain}</td>
        </tr>
        <tr>
          <th>Subdomain</th>
          <td>{subdomain || '<none>'}</td>
        </tr>
        <tr>
          <th>Upload limit</th>
          <td>
            {uploadPercentage}% ({mibUploaded} MiB / {mibLimit} MiB)
          </td>
        </tr>
        <tr>
          <th>Shorten limit</th>
          <td>
            {shortensPercentage}% ({shortensUsed} / {shortenLimit})
          </td>
        </tr>
      </tbody>
    </InfoTable>
  )
}

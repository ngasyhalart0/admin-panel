import { Button, Icon, Intent } from '@blueprintjs/core'
import React from 'react'
import { Link } from 'react-router-dom'
import styled, { css } from 'styled-components'

import CardHeader from './CardHeader'
import DomainStats from './DomainStats'
import InfoCard from './InfoCard'
import UserName from './UserName'

const StyledDomain = styled(({ unused, ...rest }) => <InfoCard {...rest} />)`
  display: flex;

  &:first-child {
    margin-top: 0;
  }

  ${(props) =>
    props.unused &&
    css`
      background: hsl(50, 100%, 95%);
      box-shadow: none;
      border: solid 1px hsl(50, 100%, 85%);
    `};
`

const Owner = styled.div`
  display: flex;
  align-items: center;
  margin-top: 0.5em;

  & div {
    margin-left: 0.25em;
  }
`

const DomainID = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 2em;
  margin-right: 0.5em;
  font-weight: 800;
  font-size: 2em;
`

const DomainContent = styled.div`
  display: flex;
  flex-flow: column nowrap;
`

const Tools = styled.div`
  padding: 1rem 0 0 0;

  button:not(:last-child) {
    margin-right: 0.5em;
  }
`

export default class Domain extends React.Component {
  state = {
    selectedUser: null,
  }

  render() {
    const { userFacing = false } = this.props
    const {
      adminOnly,
      domain,
      official,
      stats,
      publicStats,
      id,
      owner,
    } = this.props.domain

    const verifiedIcon = official ? (
      <Icon
        intent={Intent.PRIMARY}
        icon="endorsed"
        title="Official domain"
        style={{ marginLeft: '0.25em' }}
      />
    ) : null

    const adminOnlyIcon = adminOnly ? (
      <Icon
        icon="star"
        title="Admin only"
        style={{
          color: '#106ba3',
          marginLeft: '0.25em',
          opacity: 0.5,
          transform: 'translateY(-1px)',
        }}
      />
    ) : null

    const ownerInformation =
      owner != null ? (
        <Owner>
          Owned by
          <Link to={`/user/${owner.id}`}>
            <UserName user={owner} />
          </Link>
        </Owner>
      ) : null

    const tools = userFacing ? null : (
      <Tools>
        {id === 0 ? null : (
          <Button
            intent={Intent.DANGER}
            icon="trash"
            onClick={this.handleDeleteClick}
            small
          />
        )}
        <Button icon="edit" onClick={this.handleEditClick} small />
      </Tools>
    )

    return (
      <StyledDomain unused={!userFacing && stats.users === 0}>
        {userFacing ? null : <DomainID>{id}</DomainID>}
        <DomainContent>
          <CardHeader>
            {domain}
            {verifiedIcon}
            {adminOnlyIcon}
          </CardHeader>
          <DomainStats stats={stats} publicStats={publicStats} />
          {ownerInformation}
          {tools}
        </DomainContent>
      </StyledDomain>
    )
  }

  handleDeleteClick = () => {
    if (this.props.onDelete) {
      this.props.onDelete()
    }
  }

  handleEditClick = () => {
    if (this.props.onEdit) {
      this.props.onEdit()
    }
  }

  handleUserSelect = (user) => {
    this.setState({ selectedUser: user })
  }
}

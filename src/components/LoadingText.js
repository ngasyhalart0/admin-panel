import { Spinner } from '@blueprintjs/core'
import React from 'react'
import styled from 'styled-components'
import 'styled-components/macro'

const StyledLoadingText = styled.div`
  margin: 1rem 0;
  display: flex;
`

export default function LoadingText({ children }) {
  return (
    <StyledLoadingText>
      <Spinner size={Spinner.SIZE_SMALL} />
      <div css="margin-left: 0.75rem">{children}</div>
    </StyledLoadingText>
  )
}

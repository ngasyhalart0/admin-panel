import React from 'react'
import { Switch } from '@blueprintjs/core'

import withField from './withField'

export default withField(Switch, (Component, { field }, props) => (
  <Component {...field} checked={field.value} {...props} />
))

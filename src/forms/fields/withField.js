import React from 'react'
import { Field } from 'formik'

const defaultRenderer = (Component, { field }, props) => (
  <Component {...field} {...props} />
)

/**
 * Renders a component from passed props and <Field/> props. The default renderer
 * applies the <Field/> props onto the component, along with any additional
 * props, which makes the event listeners attach automatically.
 * @name WithFieldRenderer
 * @function
 * @param {React.Component} Component The component to render.
 * @param {object} fieldProps The field props provided by <Field/>'s render function.
 * @param {object} props Any additional props.
 */

/**
 * A higher order component that wraps a component with a <Field/> and
 * automatically provides the event handler props such as onChange.
 * @param {React.Component} component
 * @param {WithFieldRenderer} renderer
 */
export default function withField(component, renderer = defaultRenderer) {
  return ({ name, ...props }) => (
    <Field name={name}>
      {(fieldProps) => renderer(component, fieldProps, props)}
    </Field>
  )
}

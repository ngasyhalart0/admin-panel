export class FetchError extends Error {
  constructor(resp, body) {
    super(`HTTP ${resp.status}`)
    this.resp = resp
    this.body = body
  }

  toString() {
    if (this.body.message != null) {
      return this.body.message
    }

    return `HTTP ${this.resp.status} (${this.resp.statusText})`
  }
}

export default async function strictFetch(url, options) {
  const resp = await fetch(url, options)

  if (!resp.ok) {
    throw new FetchError(resp, await resp.json())
  }

  return resp
}

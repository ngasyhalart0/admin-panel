import React from 'react'
import Domain from '../api/domain'
import DomainListing from '../components/DomainListing'
import LoadingText from '../components/LoadingText'
import { failure } from '../toaster'

export default class MyDomains extends React.Component {
  state = {
    domains: null,
    loading: true,
  }

  componentDidMount() {
    this.fetch()
  }

  async fetch() {
    this.setState({ loading: true })
    await this.load()
    this.setState({ loading: false })
  }

  async load() {
    try {
      this.setState({
        domains: await Domain.fetchMine(),
      })
    } catch (err) {
      failure(`Failed to load your domains: ${err}`)
    }
  }

  render() {
    if (this.state.loading || this.state.domains == null) {
      return <LoadingText>Loading your domains...</LoadingText>
    }

    const domains = this.state.domains

    if (domains.length === 0) {
      return "You don't own any domains."
    }

    return <DomainListing userFacing domains={domains} />
  }
}

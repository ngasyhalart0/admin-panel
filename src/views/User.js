import { Button, ButtonGroup, Intent, Tag } from '@blueprintjs/core'
import React from 'react'

import Buttons from '../components/Buttons'
import LoadingText from '../components/LoadingText'
import User from '../api/user'
import UserEditDialog from '../components/UserEditDialog'
import UserDeleteAlert from '../components/UserDeleteAlert'
import UserInfoTable from '../components/UserInfoTable'
import { failure, success } from '../toaster'

export default class UserPage extends React.Component {
  state = {
    deleteAlertOpen: false,
    editDialogOpen: false,
    loading: false,
    loadingActivation: false,
    loadingDeletion: false,
    user: null,
  }

  componentDidMount() {
    this.load()
  }

  load() {
    this.setState({ loading: true })
    this.fetch()
    this.setState({ loading: false })
  }

  async fetch() {
    const user = await User.fetchById(this.props.match.params.id)
    this.setState({ user })
  }

  renderModals() {
    if (this.state.user == null) {
      return null
    }

    return (
      <>
        <UserDeleteAlert
          user={this.state.user}
          onCancel={this.handleDeleteAlertCancel}
          onConfirm={this.handleDeleteAlertConfirm}
          isOpen={this.state.deleteAlertOpen}
        />
        <UserEditDialog
          user={this.state.user}
          isOpen={this.state.editDialogOpen}
          onClose={this.handleUserEditDialogClose}
          onSubmit={this.handleUserEditDialogSubmit}
        />
      </>
    )
  }

  renderButtons() {
    const { active, admin } = this.state.user

    return (
      <>
        <Buttons>
          <Button icon="edit" onClick={this.handleEditButtonClick}>
            Edit
          </Button>
        </Buttons>
        <Buttons>
          <ButtonGroup>
            <Button
              intent={active ? Intent.DANGER : Intent.SUCCESS}
              icon={active ? 'disable' : 'tick'}
              loading={this.state.loadingActivation}
              onClick={this.handleChangeActivationStateClick}
              disabled={admin}
            >
              {active ? 'Deactivate' : 'Activate now'}
            </Button>
            {!active ? (
              <Button icon="envelope" onClick={this.handleSendEmail}>
                Send activation email
              </Button>
            ) : null}
            <Button
              intent={Intent.DANGER}
              icon="trash"
              loading={this.state.loadingDeletion}
              onClick={this.handleDelete}
              disabled={admin}
            >
              Delete
            </Button>
          </ButtonGroup>
        </Buttons>
      </>
    )
  }

  render() {
    const { user } = this.state

    if (this.state.loading || user == null) {
      return <LoadingText>Loading user...</LoadingText>
    }

    const tags = []
    if (user.admin) {
      tags.push(
        <Tag key="admin" intent={Intent.PRIMARY} icon="star">
          Admin
        </Tag>
      )
    }

    return (
      <>
        <h2>
          {user.name} (<code>{user.id}</code>)
        </h2>
        {tags}
        <UserInfoTable user={user} />
        {this.renderButtons()}
        {this.renderModals()}
      </>
    )
  }

  handleUserEditDialogClose = () => {
    this.setState({
      editDialogOpen: false,
    })
  }

  handleUserEditDialogSubmit = async (values, { setSubmitting }) => {
    const { user } = this.state

    try {
      await user.edit(values)
      success(`Edited ${user.name}.`)
      this.setState({ editDialogOpen: false }, this.load)
    } catch (err) {
      failure(`Failed to edit ${user.name}: ${err}`)
    } finally {
      setSubmitting(false)
    }
  }

  handleEditButtonClick = () => {
    this.setState({
      editDialogOpen: true,
    })
  }

  handleChangeActivationStateClick = async () => {
    this.setState({ loadingActivation: true })

    const user = this.state.user

    try {
      await user.changeActivationState(!user.active)
      success(`Toggled activation state of ${user.name}.`)
    } catch (err) {
      failure(`Failed to change activation state: ${err}`)
    }

    this.setState({ loadingActivation: false })

    this.load()
  }

  handleDelete = async () => {
    this.setState({ deleteAlertOpen: true })
  }

  handleDeleteAlertCancel = () => {
    this.setState({ deleteAlertOpen: false })
  }

  handleDeleteAlertConfirm = async () => {
    this.setState({ loadingDeletion: true })

    const user = this.state.user

    try {
      await user.delete()
      success(`Deleted ${user.name}.`, 'trash')
      this.props.history.push('/users')
    } catch (err) {
      failure(`Failed to delete ${user.name}: ${err}`)
    }
  }

  handleSendEmail = async () => {
    const user = this.state.user

    try {
      await user.sendEmail()
      success(`Sent email to ${user.name}.`, 'envelope')
    } catch (err) {
      failure(`Failed to send email: ${err}`)
    }
  }
}

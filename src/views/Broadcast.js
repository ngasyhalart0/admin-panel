import React from 'react'
import { Formik } from 'formik'

import BroadcastForm from '../forms/BroadcastForm'
import request from '../api'
import { failure, success } from '../toaster'

export default function Broadcast() {
  async function handleSubmit(formData, { setSubmitting }) {
    try {
      await request('/api/admin/broadcast', {
        body: JSON.stringify(formData),
        method: 'POST',
      })

      success('Broadcasted message.', 'headset')
    } catch (err) {
      failure(`Failed to broadcast: ${err}`)
    } finally {
      setSubmitting(false)
    }
  }

  return (
    <>
      <h2>Broadcast</h2>
      <p>Broadcast an email to all users:</p>

      <Formik
        initialValues={{ body: '', subject: '' }}
        component={BroadcastForm}
        onSubmit={handleSubmit}
      />
    </>
  )
}

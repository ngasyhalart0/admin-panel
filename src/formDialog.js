import React from 'react'
import { Dialog } from '@blueprintjs/core'
import { Formik } from 'formik'

import DialogContent from './components/DialogContent'

export default function formDialog(FormComponent, options) {
  return ({ onSubmit, ...providedProps }) => {
    const { dialogProps, initialValues } = options(providedProps)
    return (
      <Dialog {...dialogProps} {...providedProps}>
        <DialogContent>
          <Formik
            initialValues={initialValues}
            component={FormComponent}
            onSubmit={onSubmit}
          />
        </DialogContent>
      </Dialog>
    )
  }
}

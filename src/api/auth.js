import request from './request'
import User from './user'

export const AuthenticationState = {
  ADMIN: 'ADMIN',
  INVALID: 'INVALID',
  USER: 'USER',
}

export async function login(token) {
  var user

  try {
    user = new User(await request('/api/profile', { token }))
  } catch (error) {
    return {
      error,
      state: AuthenticationState.INVALID,
      user: null,
    }
  }

  return {
    error: null,
    state: user.admin ? AuthenticationState.ADMIN : AuthenticationState.USER,
    user,
  }
}

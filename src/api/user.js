import { query, transformKeys } from '../util'
import Domain from './domain'
import { Limits } from './limits'
import request from './request'

class User {
  /**
   * Fetches a user by ID.
   */
  static async fetchById(id) {
    const info = await request(`/api/admin/users/${id}`)
    return new User(info)
  }

  /**
   * Searches for users.
   * @param params The search parameters.
   */
  static async search(params) {
    const result = await request(`/api/admin/users/search` + query(params))
    return {
      pagination: result.pagination,
      results: result.results.map((user) => new User(user)),
    }
  }

  constructor(payload) {
    const {
      user_id: id,
      username: name,
      active,
      admin,
      domain,
      subdomain,
      email,
      paranoid,
      consented,
    } = payload

    Object.assign(this, {
      active,
      admin,
      consented,
      domain,
      email,
      id,
      name,
      paranoid,
      subdomain,
    })

    if (payload.limits) {
      this.limits = new Limits(payload.limits)
    }
  }

  /**
   * Deactivates this user.
   */
  deactivate() {
    return this.changeActivationState(false)
  }

  activate() {
    return this.changeActivationState(true)
  }

  async edit(options) {
    const payload = transformKeys(options, {
      shortenLimit: 'shorten_limit',
      uploadLimit: 'upload_limit',
    })
    const route = `/api/admin/user/${this.id}`

    await request(route, {
      body: JSON.stringify(payload),
      method: 'PATCH',
    })
  }

  async changeActivationState(state) {
    const route = `/api/admin/${state ? 'activate' : 'deactivate'}/${this.id}`
    await request(route, { method: 'POST' })
  }

  async delete() {
    await request(`/api/admin/user/${this.id}`, { method: 'DELETE' })
  }

  async fetchDomain() {
    if (typeof this.domain !== 'number') {
      return
    }

    this.domain = await Domain.fetchById(this.domain)
  }

  /**
   * Sends an email to the user containing a link to activate their account.
   */
  async sendEmail() {
    await request(`/api/admin/activate_email/${this.id}`, { method: 'POST' })
  }
}

export default User

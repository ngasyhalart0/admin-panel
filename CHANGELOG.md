# Changelog

## v2.6.0 (Four)

- Added support for searching and paginating domains.
- Added custom font stacks.
- Added support for admin settings.
- Improved how the total file size for domains were displayed.
- Improved how headers were spaced relative to the top of the page.
- Updated dependencies.
- Switched back to npm.

## v2.5.0 (Prosciutto Sandwich)

- :new: Added a user edit dialog.
  - Edit user email
  - Edit user upload and shorten limit
- :pencil: Moved from npm to Yarn.
- :wrench: Moved broadcast form to Formik.
- :wrench: Fixed spinner not showing when loading file from URL.
- :wrench: Replaced assignments with Object.assign in large constructors.

## v2.4.0 (Squashy Grapes)

- :new: Ratelimits and HTTP 429s are now handled properly with exponential
  backoff.
- :new: Added a domain edit dialog. You can now edit your domains:
  - Toggle the admin only flag
  - Toggle the official flag
  - Set the owner
- :new: Star icons are now displayed next to admin only domains.
- :pencil: The custom web font was removed in favor of faster loading times.
- :pencil: Made the loading indicator on the user detail page smaller.
- :pencil: The first domain is now editable.
- :pencil: Unused domains now appear with a less harsh tone.
- :pencil: React and React DOM have been updated to v16.5.0.
- :wrench: Fixed misaligned switch knobs.
- :wrench: Fixed spinners not animating.
- :wrench: Fixed unused domains appearing yellow on the "My Domains" page.
- :wrench: Fixed domain management tools appearing on the "My Domains" page.

## v2.3.0 (Cheesecake)

- Star icons are now displayed next to the username of an admin.
- A star icon is now displayed next to the "Admin" tag in the user detail page.
- Appropriate icons are now displayed for the statistics participation tag in
  the user detail page.
- Verified domains now have a small blue checkmark next to their name instead of
  a "Verified" tag.

- Added a header and "User is an admin" notice as appropriate to the quick
  manage menu.
- Fixed the quick manage menu crashing in production.
- The file rename button now becomes disabled when the field is empty to keep
  you from renaming a file to a blank string.
- Fixed a random render-related crash in file management.

## v2.2.0 (Squishy Cherries)

### Features

- react-scripts@2.0.0-next used by default.
  - Faster compile times!
- Redesigned vertical sidebar.
- Non-admins are now able to use the admin panel correctly.
- Domain owners can now view their domain statistics.
- The owner of a domain is now shown on the domain in the domain listing.
- You can now assign the owner of a domain through the domain listing.
- User deletion support.
- Email broadcast support.
- New user interface font: Libre Franklin
- Other usability improvements.

### Improvements

- "This user is an admin." notice on user detail page was removed.
- Domain IDs stand out more on the listing.
- "elixi.re" replaced with "elixire".

### Internal Improvements

- Internal API has been revamped to a model-based system.
- Revamped authentication logic.
- Add license to MIT.

## v2.1.0

- domains: show official tag on domains
- domains: show public stats on domains
- file management: hide "Nothing is here." while loading domains
- file management: disable type changer while loading domains
- ignore config.json, config.example.json now provided
- add configurable basename for router in config.json
- file management: fix domain selector overflowing

## v2.0.0

Elixire Admin Panel 2.0

- Full rewrite in React.
- Full management features:
  - File management.
  - User management.
  - Domain management.

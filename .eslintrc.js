module.exports = {
  extends: 'react-app',
  rules: {
    'react/sort-comp': [
      'warn',
      {
        order: [
          'static-methods',
          'lifecycle',
          'everything-else',
          'rendering',
          '/^handle.+$/',
        ],
        groups: {
          rendering: ['/^render.+$/', 'render'],
        },
      },
    ],
    'sort-keys': ['warn', 'asc', { natural: true }],
    'import/order': [
      'warn',
      {
        groups: [
          'builtin',
          'external',
          ['internal', 'parent', 'sibling', 'index'],
        ],
      },
    ],
  },
}
